package com.rahasak.zio4s

import com.typesafe.config.{Config, ConfigFactory}
import slick.jdbc.H2Profile.api._
import slick.jdbc.{JdbcBackend, JdbcProfile}
import slick.lifted.ProvenShape
import zio.{Has, IO, Task, UIO, ULayer, ZIO, ZLayer, ZManaged}

case class User(name: String, email: String)

object UsersTable {
  class Users(tag: Tag) extends Table[User](tag, "users") {
    def id: Rep[Long] = column[Long]("id", O.PrimaryKey, O.AutoInc)

    def name: Rep[String] = column[String]("name")

    def email: Rep[String] = column[String]("email")

    def * : ProvenShape[User] = (name, email) <> ((User.apply _).tupled, User.unapply)
  }

  val table = TableQuery[UsersTable.Users]
}

object DatabaseProvider {
  type DatabaseProviderEnv = Has[DatabaseProvider.Service]

  trait Service {
    def db: UIO[JdbcBackend#Database]

    def profile: UIO[JdbcProfile]
  }

  val live: ZLayer[Has[Config] with Has[JdbcProfile], Throwable, Has[Service]] = ZLayer.fromServicesManaged[Config, JdbcProfile, Any, Throwable, Service] { (cfg: Config, prof: JdbcProfile) =>
    ZManaged
      .make(ZIO.effect(prof.backend.Database.forConfig("", cfg)))(db => ZIO.effectTotal(db.close()))
      .map { d =>
        new Service {
          override def db: UIO[JdbcBackend#Database] = ZIO.effectTotal(d)

          override def profile: UIO[JdbcProfile] = ZIO.effectTotal(prof)
        }
      }
  }
}

object Repository {
  type RepositoryEnv = Has[Repository.Service]

  trait Service {
    def init(): IO[Throwable, Unit]

    def create(user: User): IO[Throwable, Int]

    def find(name: String): IO[Throwable, Option[User]]
  }

  val live: ZLayer[Has[DatabaseProvider.Service], Throwable, Has[Service]] = ZLayer.fromServiceM { dbProvider =>
    dbProvider.profile.map { profile =>
      import profile.api._

      new Service {
        private val users = UsersTable.table

        override def init: IO[Throwable, Unit] = {
          val query = users.schema.createIfNotExists
          for {
            d <- dbProvider.db
            r <- ZIO.fromFuture(_ => d.run(query))
          } yield r
        }

        override def create(user: User): IO[Throwable, Int] = {
          val query = users += user
          for {
            d <- dbProvider.db
            r <- ZIO.fromFuture(_ => d.run(query))
          } yield r
        }

        override def find(name: String): IO[Throwable, Option[User]] = {
          val query = users.filter(_.name === name).result.headOption
          for {
            d <- dbProvider.db
            r <- ZIO.fromFuture(_ => d.run(query))
          } yield r
        }
      }
    }
  }
}

object Notifier {
  type NotifierEnv = Has[Notifier.Service]

  trait Service {
    def notify(user: User, message: String): Task[Unit]
  }

  val live: ULayer[Has[Service]] = ZLayer.succeed(new Service {
    override def notify(user: User, message: String): Task[Unit] = {
      Task {
        println(s"sending email, $message to, $user")
      }
    }
  })

  def notify(user: User, message: String): ZIO[Has[Notifier.Service], Throwable, Unit] = ZIO.accessM(_.get.notify(user, message))
}


object Subscriber {
  type SubscriberEnv = Has[Subscriber.Service]

  trait Service {
    def subscribe(user: User): ZIO[Any, Throwable, User]
  }

  val live: ZLayer[Has[Notifier.Service] with Has[Repository.Service], Nothing, Has[Service]] =
    ZLayer.fromServices[Notifier.Service, Repository.Service, Subscriber.Service] { (notifier, repository) =>
      new Service {
        override def subscribe(user: User): ZIO[Any, Throwable, User] = {
          for {
            _ <- repository.init()
            _ <- repository.create(user)
            _ <- repository.find("rahasak")
            - <- notifier.notify(user, s"hello ${user.name}")
          } yield user
        }
      }
    }

  def subscribe(u: User): ZIO[Has[Subscriber.Service], Throwable, User] = ZIO.accessM(_.get.subscribe(u))
}

object Zlayer extends zio.App {
  override def run(args: List[String]) = {
    // database provider dependencies
    val rootConfig: Config = ConfigFactory.load()
    val dbConfigLayer = ZLayer.succeed(rootConfig.getConfig("h2mem1"))
    val dbProviderLayer = ZLayer.succeed(slick.jdbc.H2Profile.asInstanceOf[JdbcProfile])

    // dependency graph
    val layers = (dbConfigLayer ++ dbProviderLayer) >>>
      DatabaseProvider.live >>>
      (Repository.live ++ Notifier.live) >>>
      Subscriber.live

    // handle subscription
    Subscriber.subscribe(User("rahasak", "hello@rahasak.com"))
      .provideLayer(layers)
      .map(u => println(s"subscription completed, $u"))
      .exitCode
  }
}
