package com.rahasak.zio4s

import zio.{IO, UIO, ZIO}
import zio.console._

object Zio extends App {
  val success: IO[Nothing, Int] = ZIO.succeed(42)
  println(zio.Runtime.default.unsafeRun(success))

  val fail: IO[String, Nothing] = ZIO.fail("ZIO Error")
  println(zio.Runtime.default.unsafeRun(fail))
}

object ZioApp extends zio.App {
  val success: IO[Nothing, Int] = ZIO.succeed(42)
  val fail: IO[String, Nothing] = ZIO.fail("ZIO Error")

  def printLine(s: String) = putStrLn(s"$s")
  def readLine() = getStrLn

  val z = for {
    _ <- putStrLn("enter your name \n")
    l <- readLine()
    _ <- putStrLn(l)
  } yield l

  override def run(args: List[String]) = {
    //success.map(a => println(a)).exitCode
    //z.exitCode
    z.exitCode
  }
}
