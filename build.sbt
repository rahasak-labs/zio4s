name := "zio4s"

version := "1.0"

scalaVersion := "2.12.14"

libraryDependencies ++= {

  Seq(
    "dev.zio"                 %% "zio"                    % "1.0.4-2",
    "com.typesafe.slick"      %% "slick"                  % "3.3.2",
    "com.h2database"          % "h2"                      % "1.4.192",
    "org.slf4j"               % "slf4j-api"               % "1.7.5",
    "ch.qos.logback"          % "logback-classic"         % "1.0.9"
  )

}

resolvers ++= Seq(
  "Typesafe repository" at "http://repo.typesafe.com/typesafe/releases/"
)

assemblyMergeStrategy in assembly := {
  case PathList(ps @ _*) if ps.last endsWith ".properties" => MergeStrategy.first
  case PathList(ps @ _*) if ps.last endsWith "module-info.class" => MergeStrategy.first
  case "module-info.class" => MergeStrategy.discard
  case x =>
    val oldStrategy = (assemblyMergeStrategy in assembly).value
    oldStrategy(x)
}
