FROM oracle/graalvm-ce:20.1.0

MAINTAINER Eranga Bandara (erangaeb@gmail.com)

# install native-image
RUN gu install native-image

# work dir on /app
WORKDIR /app

# copy file
ADD target/scala-2.11/etcd4s-api-assembly-1.0.jar etcd4s.jar

# command
RUN native-image \
 -H:+ReportExceptionStackTraces \
 -H:+TraceClassInitialization \
 -Dio.netty.noUnsafe=true \
 --static \
 --verbose \
 --no-fallback \
 --allow-incomplete-classpath \
 --report-unsupported-elements-at-runtime \
 -jar etcd4s.jar etcd4s

# command
ENTRYPOINT ["/app/etcd4s"]
